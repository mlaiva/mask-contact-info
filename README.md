Prefix:
Program has been done using Java 1.8 and Maven as build tool on Eclipse IDE. Junit 5.x is used to write the test cases. No external libraries were used to attain the required result.

Assumptions:
Bill will read the information from the front end and pass the information without validating to utils method in a application called “mask-contact-info”. Once the information is received from the front-end application, validation and masking will be done in mask-contact-info app. In case of any exceptions mask-contact-app will throw exception back to Bill. Bill has to take care of exception handling.
 
 
Scenarios Covered

Once the program starts, it will be asking for the user to input their Email or 10 digit Phone Number, I have used util.Scanner this will be the replacement for front end. Since it has been mentioned that Front End has no validation, we have allowed the same, and no validations are done at that moment.


•	Null, Empty/whitespace and 80 char limit Scenario
Once the user enters his or her data, the first validation will be to check if it’s null or empty/whitespace or data is within 80 max char limit.  If that condition fails, we will throw exception as InvalidDataException with a message “Given data is empty or you have exceeded 80 char limit for ContactType”

•	Determine Input data has only Numeric
Assume user inputs some data, we will analyze and if the input String contains only Numeric, it is identified as “ContactType” as Phone. In order to make decision and to keep the performance better, we have asked User to enter only 10 digit of phone number, but we will allow user to enter lesser or higher digits, more on this scenario are as to follow.

Based on the determined ContactType more validations are done under validate method using EMAIL_REGEX and PHONE_REGEX. 

•	EMAIL Detailed Validation
If the user input does not contains only numeric, we will assume that to be of Email and we will proceed further to do more detailed validation using EMAIL_REGEX. If the validation fails, we will throw new InvalidDataException with message “Invalid Email Provided”

private static final String EMAIL_REGEX = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";

Simple light weight Regex with is also compliant with RFC822 email standard should take care of most of email validation. Since we are not using any external libraries to do the email validation, keeping the performance and use case in mind, mentioned Regex was chosen.

•	Phone Number Detailed Validation
If user input has only numeric, we will assume that to be of Phone detail and we will proceed to further to do more detailed validation using PHONE_REGEX. If the validation fails, we will throw InvalidDataException with message “Invalid Phone Number Provided”

private static final String PHONE_REGEX  = "^[0-9]{10}$";
•	^ Matches the beginning of the string (or line if multiline)
•	[ 0-9] Range. Matched a character in the range 0 to 9
•	{10} Quantifier, match 10 of preceding token
•	$ Matches the end of the string (or line if multiline)

Once detailed validation is passed, we will proceed to mask the data as per the requirement.
By this time, we already know the data entered is of Email or Phone so we mask it based on ContactType using if/else condition.

•	EMAIL Masking

Masking is done by splitting the data in to CharArray(). Now we have split it and know the index we will use Array.fill and specific the index to mask it using “*” (char val). For Email, Start index will be always one and end index will be “@ - 1 “ and fill value is “*”





•	Phone Masking
Masking is done based on the similar functionality for phone as well. To determine the start and end index , we find middle index by dividing phoneNumlength/2. From this we will derive start index as middleIndex – 2 and end index as middleIndex + 1 and mask accordingly.

We will print out the data in the Console which will be persisted in DB later.



Recommendation to Bill:
1.	Util method, won’t allow null, empty or char exceeding than 80 char limit. When the application throws error back caller (Bill’s method) he should be able to take care of the exception and communicate same to the end user via front end application.
2.	Util method, will only take exactly1 0 digits for phone number, in case of fewer or higher digits, the exception needs to be handled by Bill. In longer term Front end application should have drop down to choose either Email or Phone number,  In that way we can have more control over the validation of the input data which we will receive in mask-contact-info application.
3.	Util method, uses RFC 822 as a standard to validate email address.Though it has 80 char limit, it will not allow user to enter multiple emails by the user.
