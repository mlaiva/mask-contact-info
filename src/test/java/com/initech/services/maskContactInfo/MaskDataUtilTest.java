/**
 * 
 */
package com.initech.services.maskContactInfo;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.initech.services.mask_contact_info.exception.InvalidDataException;
import com.initech.services.mask_contact_info.utils.MaskDataUtil;

@TestInstance(Lifecycle.PER_CLASS)
public class MaskDataUtilTest {
	
	/**
	 * Test case to validate valid email and Phone Number
	 */
	@Test
	public void testMaskValidContactInformation() throws InvalidDataException {
			assertEquals("j******e@reditt.com", MaskDataUtil.maskContactInformation("john.doe@reditt.com"));
			assertEquals("784***5478", MaskDataUtil.maskContactInformation("7845415478"));
	}
	
	/**
	 * Test case to validate input data null or empty
	 */
	@Test
	public void testMaskNullContactInformation() {
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation(null));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation(""));
	}
	
	/**
	 * Test case to validate Invalid Email
	 * with @ entered 2 times in input
	 * with white space in the email
	 * with no @ in the email
	 * with no mention of domain
	 * with missing . in the domain
	 * with website url instead of email
	 */
	@Test
	public void testMaskInvalidEamilContactInformation() {
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("@@reditt.com"));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("   @reditt.com"));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("reditt.com"));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("reddit"));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("jhone.doeredditcom"));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("www.reddit.com"));
	}
	
	
	/**
	 * Test case to validate Invalid Phone Number
	 * with less than 10 digit
	 * with more than 10 digit
	 * with  white spaces 
	 * with special characters and alphanumeric 
	 */
	@Test
	public void testMaskInvalidPhoneContactInformation() {
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("11111"));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("7846521547896545"));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("   7851  "));
		assertThrows(InvalidDataException.class, () -> MaskDataUtil.maskContactInformation("478@#$54adh"));
	}
}
