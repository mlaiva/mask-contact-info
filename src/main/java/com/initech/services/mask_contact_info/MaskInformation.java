package com.initech.services.mask_contact_info;

import java.util.Scanner;

import com.initech.services.mask_contact_info.exception.InvalidDataException;
import com.initech.services.mask_contact_info.utils.MaskDataUtil;

public class MaskInformation 
{
    public static void main( String[] args ) throws InvalidDataException
    {
    	System.out.println("Enter your Email or 10 Digit Phone Number and Press Enter: ");
    	Scanner scanner = new Scanner(System.in);
    	String contactInformation = scanner.nextLine();
    	scanner.close();
    	
    	try {
    		MaskDataUtil.maskContactInformation(contactInformation);
		} catch (InvalidDataException e) {
			e.printStackTrace();
		}
    	
    }
}
