package com.initech.services.mask_contact_info.constants;

public enum ContactType {
	EMAIL,
	PHONE;
}
