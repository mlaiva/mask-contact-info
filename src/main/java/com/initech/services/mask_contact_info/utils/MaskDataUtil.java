package com.initech.services.mask_contact_info.utils;

import java.util.Arrays;

import com.initech.services.mask_contact_info.constants.ContactType;
import com.initech.services.mask_contact_info.exception.InvalidDataException;

public class MaskDataUtil {
	
	private static final String EMAIL_REGEX = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";
	private static final String PHONE_REGEX  = "^[0-9]{10}$";
	
	/**
	 * step 1:performs null check 
	 * Step 2:determines contact type 
	 * Step 3:applies type specific validation
	 * Step 4:performs the masking to contact information according to requirement
	 * 
	 * @param contactInformation
	 * @return 
	 * @throws InvalidDataException
	 */
	public static String maskContactInformation(String contactInformation) throws InvalidDataException {
		if(contactInformation == null || contactInformation.isBlank() || contactInformation.length() >80) {
			throw new InvalidDataException("Given data is empty or you have exceeded 80 char limit for ContactType");
		}
		
		ContactType contactType = determineContactType(contactInformation);
		validate(contactInformation,contactType);

		if(contactType ==ContactType.EMAIL) {
			int endIndex = contactInformation.indexOf("@") - 1;
			return mask(contactInformation,1,endIndex);
		} else {
			int totalPhNumLength = contactInformation.length();
			int middleIndex = totalPhNumLength / 2;
			return mask(contactInformation, middleIndex - 2, middleIndex + 1);
		}
	}
	
	/**
	 * to determine the contact information either PHONE or EMAIL
	 * @param contactInformation
	 * @return
	 */
	public static ContactType determineContactType(String contactInformation) {
		return contactInformation.chars().allMatch(Character::isDigit) ? ContactType.PHONE : ContactType.EMAIL;
	}
	
	/**
	 * 
	 * @param contactInformation
	 * @param contactType
	 * @throws InvalidDataException
	 */
	public static void validate(String contactInformation, ContactType contactType)throws InvalidDataException {
	
		if(contactType == ContactType.PHONE && !contactInformation.matches(PHONE_REGEX)) {
			throw new InvalidDataException("Invalid Phone Number Provided!");
		} 
		if(contactType == ContactType.EMAIL && !contactInformation.matches(EMAIL_REGEX)) {
			throw new InvalidDataException("Invalid Email Provided!");
		}
	}
	/**
	 * to mask the data based on start and end index of string
	 * @param data
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	public static String mask(String data, int startIndex, int endIndex) {
		char[] arry = data.toCharArray();
		Arrays.fill(arry, startIndex, endIndex, '*');
		data = new String(arry);
		System.out.println("Masked contact information for database :" + data);
		return data;
	}

}
