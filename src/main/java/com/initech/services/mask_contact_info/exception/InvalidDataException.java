package com.initech.services.mask_contact_info.exception;

public class InvalidDataException extends Exception {
	
	private static final long serialVersionUID = 1l;

	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidDataException(String message) {
		super(message);
	}
	
	

}
